# docker
Usage
- create container by run docker-compose up -d php70-mysql
- edit apache config files > sites/app.conf
- change container name in .envfiles


Common Docker command
create container:
docker-compose up -d [php56-mysql/php70-mysql/php70-mongo]

start contianer:
docker start [name]

enter shell:
docker exec -it [name] bash  

copy files contianer:
docker cp [name]:/etc/httpd/conf.d/ tmp
