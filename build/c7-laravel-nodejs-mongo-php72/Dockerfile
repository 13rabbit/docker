FROM 13rabbit/c7-systemd
MAINTAINER "Natchapon Futragoon" <natchapon.f@gmail.com>

RUN \
    yum -y install --setopt=tsflags=nodocs \
    	initscripts \
    	yum-utils \
		wget git curl \
    && rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    && rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm \
    && curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -

COPY files/mongodb-org-3.2.repo /etc/yum.repos.d/mongodb-org-3.2.repo 

RUN \
	mkdir -p /var/www/api/public \
	&& mkdir -p /var/www/control/public \
	&& mkdir -p /etc/httpd/conf.d/ \
	&& mkdir -p /data/db \
	&& mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor /var/www/html

RUN \
	yum -y install --setopt=tsflags=nodocs \
	php72w php72w-mbstring php72w-devel php72w-pdo php72w-mbstring php72w-pear php72w-mcrypt php72w-opcache php72w-pecl-xdebug

RUN \
	yum -y install --setopt=tsflags=nodocs \
	httpd \
	nodejs \
	mongodb-org \
	php-dom gcc openssl-devel openssh-server bzip2\
	mlocate git-all unzip logrotate\
	&& pecl install mongodb \
	&& curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

COPY files/app.conf /etc/httpd/conf.d/app.conf
COPY files/mongodb.ini /etc/php.d/mongodb.ini
COPY files/imcservice.service /etc/systemd/system/imcservice.service
COPY files/mongod-respawn /etc/init.d/mongod
COPY files/logrotate/mongod /etc/logrotate.d/mongod

RUN \
    chmod +x /etc/init.d/mongod \
    && chmod -R 777 /var/lib/mongo/ \
    && chown -R mongod:mongod /var/lib/mongo/ \
    && localedef -i en_US -f UTF-8 en_US.UTF-8 \
	&& sed -i -e 's/EnableSendfile On/EnableSendfile Off/g' /etc/httpd/conf/httpd.conf \
	&& sed -i -e 's/DocumentRoot \"\/var\/www\/html\"/DocumentRoot \"\/var\/www\"/g' /etc/httpd/conf/httpd.conf \
	&& sed -i -e 's/bindIp: 127.0.0.1/bindIp: 0.0.0.0/g' /etc/mongod.conf \
	&& sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config \
	&& sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config \
	&& echo 'root:secret' | chpasswd  \
	&& systemctl enable httpd \
	&& systemctl enable mongod


RUN npm install express ioredis socket.io -g

EXPOSE 80 27017

WORKDIR /var/www/

CMD ["/usr/sbin/init"]