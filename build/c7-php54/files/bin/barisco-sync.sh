#!/bin/bash

LOGFILE="/var/www/barisco/logs/b_rsync.log"
TIMESTAMP=$( date +%s)
echo -n "$TIMESTAMP" > $LOGFILE

rsync -zvru --delete /var/www/barisco/data/viewer/ root@sin005.park.barisco.com:/var/www/barisco/data/viewer/
rsync -zvru --delete /var/www/barisco/data/viewer/ root@ams002.park.barisco.com:/var/www/barisco/data/viewer/
rsync -zvru --delete /var/www/barisco/data/viewer/ root@dal001.park.barisco.com:/var/www/barisco/data/viewer/

LOGFILE="/var/www/barisco/logs/rsync.log"
TIMESTAMP=$( date +%s)
echo -n "$TIMESTAMP" > $LOGFILE

rsync -zvrc --delete --checksum /var/www/barisco/data/config/ root@sin005.park.barisco.com:/var/www/barisco/data/config/
rsync -zvrc --delete --checksum /var/www/barisco/data/config/ root@ams002.park.barisco.com:/var/www/barisco/data/config/
rsync -zvrc --delete --checksum /var/www/barisco/data/config/ root@dal001.park.barisco.com:/var/www/barisco/data/config/

rsync -zvrc --delete --checksum /var/www/barisco/data/abbreviations/ root@sin005.park.barisco.com:/var/www/barisco/data/abbreviations/
rsync -zvrc --delete --checksum /var/www/barisco/data/abbreviations/ root@ams002.park.barisco.com:/var/www/barisco/data/abbreviations/
rsync -zvrc --delete --checksum /var/www/barisco/data/abbreviations/ root@dal001.park.barisco.com:/var/www/barisco/data/abbreviations/

rsync -zvrc --delete --checksum /var/www/barisco/data/publications/ root@sin005.park.barisco.com:/var/www/barisco/data/publications/
rsync -zvrc --delete --checksum /var/www/barisco/data/publications/ root@ams002.park.barisco.com:/var/www/barisco/data/publications/
rsync -zvrc --delete --checksum /var/www/barisco/data/publications/ root@dal001.park.barisco.com:/var/www/barisco/data/publications/

rsync -zvrc --delete --checksum /var/www/barisco/data/domains/ root@sin005.park.barisco.com:/var/www/barisco/data/domains/
rsync -zvrc --delete --checksum /var/www/barisco/data/domains/ root@ams002.park.barisco.com:/var/www/barisco/data/domains/
rsync -zvrc --delete --checksum /var/www/barisco/data/domains/ root@dal001.park.barisco.com:/var/www/barisco/data/domains/

