FROM 13rabbit/c7-systemd
MAINTAINER "Natchapon Futragoon" <natchapon.f@gmail.com>

RUN \
    yum -y update \
    && yum -y install --setopt=tsflags=nodocs \
    initscripts \
    wget git curl yum-utils \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \ 
    http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    && curl --silent --location https://rpm.nodesource.com/setup_8.x | bash - \
    && yum-config-manager --enable remi-php56

RUN \
	mkdir -p /etc/httpd/conf.d/ \
	&& mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor /var/www/html

RUN \
    yum -y install --setopt=tsflags=nodocs \
    php \
    php-mysql \
    git \
    mlocate \
    curl \
    less \
    httpd \
    php-mbstring \
    php-mcrypt \
    php-devel \
    php-pear \
    php-opcache \
    php-pecl-xdebug \
    openssh-server \
    && yum clean all 

RUN \
	yum -y install --setopt=tsflags=nodocs \
	httpd mod_ssl\
	nodejs \
	php-dom gcc openssl-devel openssh-server bzip2\
	mlocate git-all unzip logrotate libpng-devel\
	&& pecl install mongodb \
	&& curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

COPY files/vhost.conf /etc/httpd/conf.d/vhost.conf
COPY files/mongodb.ini /etc/php.d/mongodb.ini

RUN \
  	localedef -i en_US -f UTF-8 en_US.UTF-8 \
	&& sed -i -e 's/EnableSendfile On/EnableSendfile Off/g' /etc/httpd/conf/httpd.conf \
	&& sed -i -e 's/DocumentRoot \"\/var\/www\/html\"/DocumentRoot \"\/var\/www\"/g' /etc/httpd/conf/httpd.conf \
	&& sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config \
	&& sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config \
	&& echo 'root:secret' | chpasswd  \
	&& systemctl enable httpd 

EXPOSE 80 443

WORKDIR /var/www/

CMD ["/usr/sbin/init"]